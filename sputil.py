from dotenv import load_dotenv
from os import getenv

load_dotenv()

calendar_id = int(getenv('CALENDAR_ID'))
welcome_id = int(getenv('WELCOME_ID'))
server_id = int(getenv('SERVER_ID'))
lore_id = int(getenv('LORE_ID'))
infocenter_id = int(getenv('INFOCENTER_ID'))
timeline_id = int(getenv('TIMELINE_ID'))
token = getenv('TOKEN')
gitrepo = str(getenv('REPO'))
member_role = str(getenv('MEMBER'))
pfx = str(getenv('PREFIX'))
quotelink = str(getenv('QUOTES'))


def beginsWithString(string: str, dest: str) -> bool:
    return string in dest[0:len(string)]


def matches(string: str, array: list) -> bool:
    for e in array:
        if beginsWithString(e, string):
            return True
        else:
            continue


def spaceRemover(content: str) -> str:
    if content[0] == ' ':
        content = spaceRemover(content[1:len(content)].casefold())
    return content


def getData() -> dict:
    import json

    data = json.load(open('data.json', 'r'))
    return data


def getQuotes() -> dict:
    import json

    quotes = json.load(open('quotes.json', 'r'))
    return quotes


def getCMDS() -> dict:
    import json

    cmds = json.load(open('commands.json', 'r'))
    return cmds
