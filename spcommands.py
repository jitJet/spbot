def getRandomQuote() -> str:
    from random import choice
    from sputil import getQuotes

    quotes = getQuotes()
    values = quotes['shitpost'].values()
    quote = choice(list(values))
    if quote == quotes['shitpost']['last']:
        return getRandomQuote()
    else:
        return quote


def getSpecificQuote(key: str) -> str:
    from sputil import getQuotes

    quotes = getQuotes()
    keys = list(quotes['shitpost'].keys())
    if key in keys:
        return quotes['shitpost'][key]
    else:
        return quotes['shitpost']['last']


def getHelpText() -> str:
    from sputil import getCMDS, pfx

    cmds = getCMDS()
    text = 'Commands:\n'
    for key in list(cmds.keys()):
        cmd = '%s: %s\n' % (key, cmds[key]['action'])
        cmd = '\tUsage: %s%s\n\n' % (cmd, cmds[key]['usage'] % pfx)
        text = text + cmd
    text = '%sPrefix: %s\n' % (text, pfx)
    text = '```\n%s```' % text
    return text


async def execute(self, message):
    content = message.content
    author = message.author

    # don't respond to ourselves
    if author == self.user:
        return
    else:
        from sputil import pfx, matches, beginsWithString
        from sputil import getCMDS, getData

        cmds = getCMDS()
        data = getData()
        
        if beginsWithString(pfx, content):
            from sputil import spaceRemover

            cmd = spaceRemover(content[len(pfx):])
            # Repo Command
            if cmd in list(cmds['repo']['alias']):
                from sputil import gitrepo

                await message.channel.send(data['repo'] % gitrepo)
            # Info Command
            elif cmd in list(cmds['info']['alias']):
                from sputil import server_id

                if message.guild.id == server_id:
                    from sputil import infocenter_id, lore_id, timeline_id

                    info_text = data['info'] % (infocenter_id, lore_id, timeline_id)
                    await message.channel.send(info_text)
            # Shitpost Command
            elif cmd in list(cmds['shitpost']['alias']):
                await message.channel.send(getRandomQuote())
            # Help Command
            elif cmd in list(cmds['help']['alias']):
                await message.channel.send(getHelpText())
            # Quotes Command
            elif cmd in list(cmds['quotes']['alias']):
                from sputil import quotelink
                
                await message.channel.send(data['quotes'] % quotelink)
            # Quote Command
            elif matches(cmd, list(cmds['quote']['alias'])):
                for e in list(cmds['quote']['alias']):
                    cmd = cmd.replace(e, '')
                cmd = cmd.replace(' ', '')
                quote = getSpecificQuote(cmd)
                if quote != '':
                    await message.channel.send(quote)
