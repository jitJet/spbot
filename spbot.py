import discord
from sputil import token


class MyClient(discord.Client):
    async def on_connect(self):
        print('Hello there')

    async def on_ready(self):
        print('Logged on as', self.user)
        # self.loop.create_task(cycleTrigger(self))

    async def on_disconnet(self):
        print('Shutting down', self.user)

    async def on_message(self, message):
        # don't respond to ourselves
        author = message.author
        if author == self.user:
            return
        else:
            from spcommands import execute
            await execute(self, message)

    async def on_member_join(self, member):
        from sputil import server_id

        if member.guild.id == server_id:
            from spcommands import getRandomQuote
            from sputil import welcome_id, member_role, getData
            from sputil import lore_id, timeline_id, infocenter_id

            data = getData()
            info_text = data['info'] % (infocenter_id, lore_id, timeline_id)
            wlcm = data['welcome'] % (getRandomQuote(), str(member.id), info_text)
            role = discord.utils.get(member.guild.roles, name=member_role)
            await member.add_roles(role)
            welcome_ch = self.get_channel(welcome_id)
            await welcome_ch.send(wlcm)

    async def on_error(self, event, *args, **kwargs):
        with open('err.log', 'a') as f:
            if event == 'on_message':
                f.write(f'Unhandled message: {args[0]}\n')
            else:
                raise


client = MyClient()
client.run(token)
